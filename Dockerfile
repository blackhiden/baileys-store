FROM node:latest
WORKDIR /app
VOLUME /app/node_modules
COPY . .
RUN npm install
